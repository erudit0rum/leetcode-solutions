const permutations = (npt) => {
    let recurse = (set, unset) => {
        if (unset.length == 0) {
            return set;
        }
        let branches = [];
        unset.forEach((item) => {
            let item_idx = unset.indexOf(item)
            let unset_w_out_item = [...unset.slice(0,item_idx), ...unset.slice(item_idx+1)]
            let branch = recurse(set + String(item), unset_w_out_item);

            if (typeof(branch) == 'string') {
                branches.push(branch);
            } else if (Array.isArray(branch)) {
                branches = [...branches, ...branch];
            }
        })
        return branches;
    }
    return recurse('', npt);
}

console.log(permutations([1, 2, 3, 4]))