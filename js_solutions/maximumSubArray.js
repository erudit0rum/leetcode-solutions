const maximumSubArray = (npt) => {
    let max_sub_array = ['',-900]
    for (let i = 0;i < npt.length;i++) {
        for (let j = i + 1;j < npt.length; j++) {
            let current_sub_array = [
                `${i}:${j-1}`,
                npt.slice(i,j).reduce((a,b) => {
                    return a + b;
                })
            ];
            if (current_sub_array[1] > max_sub_array[1]) {
                max_sub_array = current_sub_array;
            }
        }
    }
    return max_sub_array;
}

console.log(maximumSubArray([-2,1,-3,4,-1,2,1,-5,4]))