//Given a triangle, find the minimum path sum from top to bottom.
//Each step you may move to adjacent numbers on the row below.

/*
example input
[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]
*/

const triangle = (trngl) => {
    let removeTop = (trngl) => trngl.slice(1)
    let removeLeft = (trngl) => trngl.map(row => row.slice(1))
    let removeRight = (trngl) => trngl.map(row => row.slice(0,-1))
    let chooseLeft = (trngl) => removeRight(removeTop(trngl))
    let chooseRight = (trngl) => removeLeft(removeTop(trngl))
    let selectGreatest = (options) => {
        return options.reduce((a, b) => a.split('=')[1]+0 > b.split('=')[1]+0 ? a : b)
    }
    let decisionTree = (crnt, trngl) => {
        if (trngl.length == 0) {
            return `${crnt.join("+")}=${crnt.reduce((a,b) => a+b)}`
        }
        let branches = trngl[0].map((choice, idx) => {
            if (idx == 0) {
                return decisionTree(crnt.concat([choice]), chooseLeft(trngl))
            } else if (idx == 1) {
                return decisionTree(crnt.concat([choice]), chooseRight(trngl))
            }
        }).flat()
        return branches
    }
    return selectGreatest(decisionTree(trngl[0], trngl.slice(1)))
}

let inpt = [
    [2],
   [3,14],
  [6,5,7],
 [4,1,8,31]
]

console.log(triangle(inpt))