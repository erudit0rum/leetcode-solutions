const twoDMatricSearch = (npt, target) => {
    if (target > npt.reverse()[0][0]) {
        return false
    }
    for (let i = 0;i < npt.length;i++) {
        if (npt[i][0] > npt) {
            return false
        }
        if (npt[i+1][0] > target) {
            for (cell of npt[i]) {
                if (cell == target) {
                    return true
                }
            }
            return False
        }
    }
}

console.log(twoDMatricSearch([
    [1,   3,  5,  7],
    [10, 11, 16, 20],
    [23, 30, 34, 50]
  ], 36))