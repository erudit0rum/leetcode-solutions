const groupAnagram = (inpt) => {
    let grpd_by_srt = {};
    inpt.forEach((item) => {
        let sort = item.split('').sort().join('');
        if (!(sort in grpd_by_srt)) {
            grpd_by_srt[sort] = [];
        }
        grpd_by_srt[sort].push(item);
    })
    let out = [];
    for (sort in grpd_by_srt) {
        out.push(grpd_by_srt[sort]);
    }
    return out;
}

console.log(groupAnagram(["eat", "tea", "tan", "ate", "nat", "bat"]));