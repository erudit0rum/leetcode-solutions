"""leetcode solutions setup script."""
from setuptools import setup, Command


REQUIRES = [
    "pytest==5.2.2",
    "pytest-runner==5.2"
]

setup(
    name='leetcode-solutions',
    version='0.0.0',
    author='isaac duarte',
    packages=['solutions'],
    include_package_data=True,
    test_suite="tests",
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
