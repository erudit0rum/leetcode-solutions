import pytest
from solutions.two_sum import two_sum

def test_1_two_sum():
    nums = [3, 7, 14, 23, 2]
    target = 9
    assert two_sum(nums, target) == [1, 4], 'failed'

def test_2_two_sum():
    nums = [3, 7, 35, 12, 9]
    target = 12
    assert two_sum(nums, target) == [0, 4], 'failed'

def test_3_two_sum():
    nums = [3, 7, 14, 23, 2]
    target = 21
    assert two_sum(nums, target) == [1, 2], 'failed'