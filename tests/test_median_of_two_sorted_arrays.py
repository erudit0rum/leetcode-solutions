import pytest
from solutions.median_of_two_sorted_arrays import median_of_two_sorted_arrays

def test_1_median_of_two_sorted_arrays():
    nums_1 = [7, 14, 23, 2]
    nums_2 = [3]
    assert median_of_two_sorted_arrays(nums_1, nums_2) == 7, 'failed'

def test_2_median_of_two_sorted_arrays():
    nums_1 = [7, 13]
    nums_2 = [24, 2]
    assert median_of_two_sorted_arrays(nums_1, nums_2) == 10, 'failed'

def test_3_median_of_two_sorted_arrays():
    nums_1 = [3, 7, 14, 23, 2]
    nums_2 = [3, 7, 14, 23]
    assert median_of_two_sorted_arrays(nums_1, nums_2) == 7, 'failed'