import pytest
from solutions.longest_substring_wo_duplicates import longest_substring_wo_duplicates

def test_1_longest_substring_wo_duplicates():
    test_string = 'asddaabbasdghnffffff'
    assert longest_substring_wo_duplicates(test_string) == 'basdghnf', 'failed'

def test_2_longest_substring_wo_duplicates():
    test_string = 'aaaaaasdfghjkkkkkkkkkk'
    assert longest_substring_wo_duplicates(test_string) == 'asdfghjk', 'failed'

def test_3_longest_substring_wo_duplicates():
    test_string = 'aaaaaasdfghjkkkkkkkkkkasdfghjklzxcvbnm'
    assert longest_substring_wo_duplicates(test_string) == 'kasdfghjklzxcvbnm', 'failed'