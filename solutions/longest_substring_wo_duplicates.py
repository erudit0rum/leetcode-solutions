# Given a string, find the length of the longest substring without repeating characters.

def longest_substring_wo_duplicates(npt_strn):
    lngst_sbstrn = ''
    crnt_sbstrn = ''
    while npt_strn:
        crnt_sbstrn += npt_strn[0]
        npt_strn = npt_strn[1:]
        if len(crnt_sbstrn) > 1 and crnt_sbstrn[-1] == crnt_sbstrn[-2]:
            crnt_sbstrn = crnt_sbstrn[-1]
        if len(crnt_sbstrn) > len(lngst_sbstrn):
            lngst_sbstrn = crnt_sbstrn
    return lngst_sbstrn