# There are two sorted arrays nums1 and nums2 of size m and n respectively.
# Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
# You may assume nums1 and nums2 cannot be both empty.

def median_of_two_sorted_arrays(sum_1, sum_2):
    com_sum = sum_1 + sum_2
    com_sum.sort()
    com_len = len(com_sum)
    odd = com_len % 2 != 0
    if odd:
        return com_sum[int((com_len - 1)/2)]
    else:
        strp_len = int(com_len/2 - 1)
        mdl_tw = com_sum[strp_len:-strp_len]
        return (mdl_tw[0] + mdl_tw[1]) / 2